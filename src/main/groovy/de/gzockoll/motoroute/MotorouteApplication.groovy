package de.gzockoll.motoroute

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class MotorouteApplication {

    static void main(String[] args) {
        SpringApplication.run MotorouteApplication, args
    }
}
