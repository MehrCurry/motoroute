package de.gzockoll.motoroute

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime

import static com.google.common.base.Preconditions.checkState

@Service
@Slf4j
class Converter {
    static final LocalDateTime STARTED_AT = LocalDateTime.now()
    static final USER_HOME = System.getProperty("user.home")
    static final String OSM_FILE = "$USER_HOME/Downloads/schleswig-holstein-latest.osm.pbf"

    @PostConstruct
    run() {
        convert(OSM_FILE)
    }

    def convert(file) {
        log.info("Started at $STARTED_AT")
        def path = Paths.get(file)
        checkState(Files.exists(path), "Input file not found")
        def (String fileWithoutExt, String shortName) = getRegionName(path)
        log.debug(fileWithoutExt)
        log.debug(shortName)
    }

    def getRegionName(fileName) {
        def path = Paths.get(fileName)
        def name = path.fileName.toString()
        def fileWithoutExt = name.take(name.lastIndexOf('.osm.pbf')) - '-latest'
        def parts = name.split("-")
        def shortName = ((parts.length == 1) ? fileWithoutExttake(3) : parts[0].take(1) + parts[1].take(1)).toUpperCase()
        [fileWithoutExt, shortName]
    }

    static void main(args) {
        new Converter().convert(OSM_FILE)
    }
}
